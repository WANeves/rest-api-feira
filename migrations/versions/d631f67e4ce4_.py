"""empty message

Revision ID: d631f67e4ce4
Revises: 
Create Date: 2021-07-19 01:48:18.383293

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd631f67e4ce4'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('sub_prefeituras',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=256), nullable=True),
    sa.Column('region_5', sa.String(length=64), nullable=True),
    sa.Column('region_8', sa.String(length=64), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('distritos',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=256), nullable=True),
    sa.Column('sub_prefeituras_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['sub_prefeituras_id'], ['sub_prefeituras.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('enderecos',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('logradouro', sa.String(length=256), nullable=True),
    sa.Column('numero', sa.String(length=64), nullable=True),
    sa.Column('bairro', sa.String(length=256), nullable=True),
    sa.Column('referencia', sa.String(length=256), nullable=True),
    sa.Column('latitude', sa.String(length=256), nullable=True),
    sa.Column('longitude', sa.String(length=256), nullable=True),
    sa.Column('set_cens', sa.String(length=256), nullable=True),
    sa.Column('area_p', sa.String(length=256), nullable=True),
    sa.Column('distritos_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['distritos_id'], ['distritos.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('feiras',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=256), nullable=True),
    sa.Column('registro', sa.String(length=64), nullable=True),
    sa.Column('enderecos_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['enderecos_id'], ['enderecos.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('feiras')
    op.drop_table('enderecos')
    op.drop_table('distritos')
    op.drop_table('sub_prefeituras')
    # ### end Alembic commands ###
