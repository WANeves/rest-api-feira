import json

import pytest


def body_feira():
    body = {
        "data": {
            "type": "Feira",
            "attributes": {
                "name": "VILA FORMOSA",
                "registro": "4041-0",
                "endereco-id": 1
            }
        },
        "id": 1,
        "type": "string"
    }

    return body


def test_post_feiras(app, hostname):
    """Endpoint Feiras."""

    with app.test_client() as client:
        body = body_feira()

        response = client.post(
            f'{hostname}/feiras/',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 201
        assert "VILA FORMOSA" == data['data']['attributes']['name']
        assert "4041-0" == data['data']['attributes']['registro']


def test_get_all_feiras_empty(app, hostname):
    """Get all endpoint Feiras."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/feiras/',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert [] == data['data']


def test_patch_feiras(app, hostname, add_enderecos, add_feira):
    """Patch endpoint Feiras."""

    with app.test_client() as client:
        body = body_feira()

        body['data']['id'] = 1
        body['data']['attributes']['name'] = "VILA FORMOSA updated!"

        response = client.patch(
            f'{hostname}/feiras/1/',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert "VILA FORMOSA updated!" == data['data']['attributes']['name']
