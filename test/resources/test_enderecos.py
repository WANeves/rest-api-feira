import json

import pytest


def body_endereco():
    body = {
        "data": {
            "type": "Endereco",
            "attributes": {
                "logradouro": "RUA MARAGOJIPE",
                "numero": "S/N",
                "bairro": "VL FORMOSA",
                "referencia": "TV RUA PRETORIA",
                "latitude": "23558733",
                "longitude": "-46550164",
                "set-cens": "355030810000027",
                "area-p": "3550308005005",
                "distrito-id": 1
            }
        },
        "id": 1,
        "type": "string"
    }

    return body


@pytest.fixture()
def add_relationship_enderecos_feira(app, hostname, add_enderecos, add_feira):
    with app.test_client() as client:
        body = {
            "data": [
                {
                    "type": "Feira",
                    "id": 1
                }
            ]
        }

        response = client.post(
            f'{hostname}/enderecos/1/relationships/feiras',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        return data


def test_post_enderecos(app, hostname):
    """Endpoint Enderecos."""

    with app.test_client() as client:
        body = body_endereco()

        response = client.post(
            f'{hostname}/enderecos/',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 201
        assert "RUA MARAGOJIPE" == data['data']['attributes']['logradouro']


def test_get_all_enderecos_empty(app, hostname):
    """Get all endpoint Enderecos."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/enderecos/',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert [] == data['data']


def test_get_enderecos_not_found_feiras(app, hostname, add_enderecos):
    """Get endpoint Enderecos."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/enderecos/1/feiras',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert [] == data['data']


def test_post_create_relationship_with_feiras(app, hostname, add_enderecos, add_feira):
    """Get endpoint Enderecos."""

    with app.test_client() as client:
        body = {
            "data": [
                {
                    "type": "Feira",
                    "id": 1
                }
            ]
        }

        response = client.post(
            f'{hostname}/enderecos/1/relationships/feiras',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert "Relationship successfully created" == data['meta']['message']


def test_get_enderecos_with_feiras(app, hostname, add_relationship_enderecos_feira):
    """Get relationship endpoint Enderecos."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/enderecos/1/feiras',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        relationship_end = add_relationship_enderecos_feira
        assert "Relationship successfully created" == relationship_end['meta']['message']

        assert response.status_code == 200
        assert len(data['data']) > 0
        assert "Feira" == data['data'][0]['type']
        assert "VILA FORMOSA" == data['data'][0]['attributes']['name']
        assert "4041-0" == data['data'][0]['attributes']['registro']


def test_patch_enderecos(app, hostname, add_enderecos):
    """Patch endpoint Enderecos."""

    with app.test_client() as client:
        body = body_endereco()

        body['data']['id'] = 1
        body['data']['attributes']['logradouro'] = "RUA MARAGOJIPE updated!"

        response = client.patch(
            f'{hostname}/enderecos/1/',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert "RUA MARAGOJIPE updated!" == data['data']['attributes']['logradouro']
