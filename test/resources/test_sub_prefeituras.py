import json

import pytest


def body_sub_prefeitura():
    body = {
        "data": {
            "type": "SubPrefeitura",
            "attributes": {
                "name": "Mariana",
                "region-5": "C-1",
                "region-8": "C-2"
            }
        },
        "type": "string"
    }

    return body


@pytest.fixture
def add_sub_prefeitura(app, hostname):
    with app.test_client() as client:
        body = body_sub_prefeitura()

        client.post(
            f'{hostname}/sub-prefeituras/',
            data=json.dumps(body),
            content_type='application/json',
        )


@pytest.fixture()
def add_relationship_sub_prefeitura_distrito(app, hostname, add_sub_prefeitura, add_distrito):
    with app.test_client() as client:
        body = {
            "data": [
                {
                    "type": "Distrito",
                    "id": 1
                }
            ]
        }

        response = client.post(
            f'{hostname}/sub-prefeituras/1/relationships/distritos',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        return data


def test_post_sub_prefeituras(app, hostname):
    """Endpoint SubPrefeituras."""

    with app.test_client() as client:
        body = body_sub_prefeitura()

        response = client.post(
            f'{hostname}/sub-prefeituras/',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 201
        assert "Mariana" == data['data']['attributes']['name']
        assert "C-1" == data['data']['attributes']['region-5']
        assert "C-2" == data['data']['attributes']['region-8']


def test_get_sub_prefeituras_empty(app, hostname):
    """Get endpoint SubPrefeituras."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/sub-prefeituras/',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert [] == data['data']


def test_get_sub_prefeituras_not_found_distritos(app, hostname, add_sub_prefeitura):
    """Get endpoint SubPrefeituras."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/sub-prefeituras/1/distritos',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert [] == data['data']


def test_post_create_relationship_with_distritos(app, hostname, add_sub_prefeitura, add_distrito):
    """Get all endpoint SubPrefeituras."""

    with app.test_client() as client:
        body = {
            "data": [
                {
                    "type": "Distrito",
                    "id": 1
                }
            ]
        }

        response = client.post(
            f'{hostname}/sub-prefeituras/1/relationships/distritos',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert "Relationship successfully created" == data['meta']['message']


def test_get_sub_prefeituras_with_distritos(app, hostname, add_relationship_sub_prefeitura_distrito):
    """Get relationship endpoint SubPrefeituras."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/sub-prefeituras/1/distritos',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        relationship_sub = add_relationship_sub_prefeitura_distrito
        assert "Relationship successfully created" == relationship_sub['meta']['message']

        assert response.status_code == 200
        assert len(data['data']) > 0
        assert "Distrito" == data['data'][0]['type']
        assert "Centro" == data['data'][0]['attributes']['name']


def test_patch_sub_prefeituras(app, hostname, add_sub_prefeitura):
    """Patch endpoint SubPrefeituras."""

    with app.test_client() as client:
        body = body_sub_prefeitura()

        body['data']['id'] = 1
        body['data']['attributes']['name'] = "Mariana updated!"

        response = client.patch(
            f'{hostname}/sub-prefeituras/1/',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert "Mariana updated!" == data['data']['attributes']['name']
        assert "C-1" == data['data']['attributes']['region-5']
        assert "C-2" == data['data']['attributes']['region-8']
