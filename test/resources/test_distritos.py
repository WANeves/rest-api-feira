import json

import pytest


def body_distrito():
    body = {
        "data": {
            "type": "Distrito",
            "attributes": {
                "name": "Centro",
                "sub-prefeitura-id": 1
            }
        },
        "id": 1,
        "type": "string"
    }

    return body


@pytest.fixture()
def add_relationship_distrito_enderecos(app, hostname, add_distrito, add_enderecos):
    with app.test_client() as client:
        body = {
            "data": [
                {
                    "type": "Endereco",
                    "id": 1
                }
            ]
        }

        response = client.post(
            f'{hostname}/distritos/1/relationships/enderecos',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        return data


def test_post_distritos(app, hostname):
    """Endpoint Distritos."""

    with app.test_client() as client:
        body = body_distrito()

        response = client.post(
            f'{hostname}/distritos/',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 201
        assert "Centro" == data['data']['attributes']['name']


def test_get_distritos_empty(app, hostname):
    """Get all endpoint Distritos."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/distritos/',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert [] == data['data']


def test_get_distritos_not_found_enderecos(app, hostname, add_distrito):
    """Get endpoint Distritos."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/distritos/1/enderecos',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert [] == data['data']


def test_post_create_relationship_with_enderecos(app, hostname, add_distrito, add_enderecos):
    """Get endpoint Distritos."""

    with app.test_client() as client:
        body = {
            "data": [
                {
                    "type": "Endereco",
                    "id": 1
                }
            ]
        }

        response = client.post(
            f'{hostname}/distritos/1/relationships/enderecos',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert "Relationship successfully created" == data['meta']['message']


def test_get_distritos_with_enderecos(app, hostname, add_relationship_distrito_enderecos):
    """Get relationship endpoint Distritos."""

    with app.test_client() as client:
        response = client.get(
            f'{hostname}/distritos/1/enderecos',
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        relationship_distrito = add_relationship_distrito_enderecos
        assert "Relationship successfully created" == relationship_distrito['meta']['message']

        assert response.status_code == 200
        assert len(data['data']) > 0
        assert "Endereco" == data['data'][0]['type']
        assert "RUA MARAGOJIPE" == data['data'][0]['attributes']['logradouro']


def test_patch_distritos(app, hostname, add_distrito):
    """Patch endpoint Distritos."""

    with app.test_client() as client:
        body = body_distrito()

        body['data']['id'] = 1
        body['data']['attributes']['name'] = "Centro updated!"

        response = client.patch(
            f'{hostname}/distritos/1/',
            data=json.dumps(body),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())

        assert response.status_code == 200
        assert "Centro updated!" == data['data']['attributes']['name']
