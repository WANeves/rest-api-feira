import json
import os

import pytest

from app.database.db import db
from app.rest_api import create_app


@pytest.fixture
def app():
    app = create_app(is_test=True)
    app.config.from_object(
        f"app.config.{os.getenv('APPLICATION_ENV', 'Testing')}"
    )

    with app.app_context():
        db.create_all()
        yield app
        db.session.remove()  # looks like db.session.close() would work as well
        db.drop_all()


@pytest.fixture()
def hostname(app):
    prefix = app.config["PREFIX"]
    host = app.config["HOST"]
    port = app.config["PORT"]

    if host == "0.0.0.0":
        host = "localhost"

    if port:
        host = "%s:%s" % (host, port)

    return f"http://{host}{prefix}"


@pytest.fixture
def add_distrito(app, hostname):
    with app.test_client() as client:
        body = {
                "data": {
                    "type": "Distrito",
                    "attributes": {
                        "name": "Centro",
                        "sub-prefeitura-id": 1
                    }
                },
                "id": 1,
                "type": "string"
            }

        client.post(
            f'{hostname}/distritos/',
            data=json.dumps(body),
            content_type='application/json',
        )


@pytest.fixture
def add_enderecos(app, hostname):
    with app.test_client() as client:
        body = {
                "data": {
                    "type": "Endereco",
                    "attributes": {
                        "logradouro": "RUA MARAGOJIPE",
                        "numero": "S/N",
                        "bairro": "VL FORMOSA",
                        "referencia": "TV RUA PRETORIA",
                        "latitude": "23558733",
                        "longitude": "-46550164",
                        "set-cens": "355030810000027",
                        "area-p": "3550308005005",
                        "distrito-id": 1
                    }
                },
                "id": 1,
                "type": "string"
        }

        client.post(
            f'{hostname}/enderecos/',
            data=json.dumps(body),
            content_type='application/json',
        )


@pytest.fixture
def add_feira(app, hostname):
    with app.test_client() as client:
        body = {
                "data": {
                    "type": "Feira",
                    "attributes": {
                        "name": "VILA FORMOSA",
                        "registro": "4041-0",
                        "endereco-id": 1
                    }
                },
                "id": 1,
                "type": "string"
            }

        response = client.post(
            f'{hostname}/feiras/',
            data=json.dumps(body),
            content_type='application/json',
        )

        data = json.loads(response.data.decode())

        return data
