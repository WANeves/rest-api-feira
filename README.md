# REST API - Service Feiras 

## Overview
This example performs the import of a .csv file with a list of Feiras that is saved in the database. 
The API allows all CRUD actions through the swagger and its respective endpoints.

## Running with Docker-Compose

To run the server on a Docker-Compose, please execute the following from the root directory:

```bash
# starting and building the images
$ docker-compose -f docker-compose.yml up -d --build
``` 
######IMPORTANT: Run this command twice so the migration can be run on the database.

and open your browser to here:

```
http://localhost:8080/api/v1/rest/
```

View Swagger definitions:

![image info](./image/swagger.png)


## View Database Service
acess database service:

```bash
$ docker-compose exec db psql --username=admin --dbname=feira_db
```

connect in database:

```bash
feira_db=# \c feira_db

You are now connected to database "feira_db" as user "admin".
```

view tables in database:

```bash
feira_db=# \dt

            List of relations
 Schema |      Name       | Type  | Owner 
--------+-----------------+-------+-------
 public | alembic_version | table | admin
 public | distritos       | table | admin
 public | enderecos       | table | admin
 public | feiras          | table | admin
 public | sub_prefeituras | table | admin
(5 rows) 
```

execute select in table:

```bash
feira_db=# select * from sub_prefeituras;

 id |           name            | region_5 | region_8 
----+---------------------------+----------+----------
  1 | PERUS                     | Norte    | Norte 1
  2 | PIRITUBA                  | Norte    | Norte 1
  3 | FREGUESIA-BRASILANDIA     | Norte    | Norte 1
  4 | CASA VERDE-CACHOEIRINHA   | Norte    | Norte 1
  5 | SANTANA-TUCURUVI          | Norte    | Norte 2
  6 | JACANA-TREMEMBE           | Norte    | Norte 2
  7 | VILA MARIA-VILA GUILHERME | Norte    | Norte 2
  8 | LAPA                      | Oeste    | Oeste
  9 | SE                        | Centro   | Centro
 10 | BUTANTA                   | Oeste    | Oeste
 11 | PINHEIROS                 | Oeste    | Oeste
 12 | VILA MARIANA              | Sul      | Sul 1
 13 | IPIRANGA                  | Sul      | Sul 1
 14 | SANTO AMARO               | Sul      | Sul 2
 15 | JABAQUARA                 | Sul      | Sul 1
 16 | CIDADE ADEMAR             | Sul      | Sul 2
 17 | CAMPO LIMPO               | Sul      | Sul 2
 18 | M'BOI MIRIM               | Sul      | Sul 2
 19 | CAPELA DO SOCORRO         | Sul      | Sul 2
 20 | PARELHEIROS               | Sul      | Sul 2
 21 | PENHA                     | Leste    | Leste 1
 22 | ERMELINO MATARAZZO        | Leste    | Leste 2
 23 | SAO MIGUEL                | Leste    | Leste 2
 24 | ITAIM PAULISTA            | Leste    | Leste 2
 25 | MOOCA                     | Leste    | Leste 1
 26 | ARICANDUVA-FORMOSA-CARRAO | Leste    | Leste 1
 27 | ITAQUERA                  | Leste    | Leste 2
 28 | GUAIANASES                | Leste    | Leste 2
 29 | VILA PRUDENTE             | Leste    | Leste 1
 30 | SAO MATEUS                | Leste    | Leste 2
 31 | CIDADE TIRADENTES         | Leste    | Leste 2
(31 rows)

```

## Running Tests

acess bash in container:

```bash
$ docker-compose exec web sh
```

running tests with pytest:

```bash
pytest -vv --disable-warnings
```
![image info](./image/pytest.png)

## Stop services
stop all services and remove volumes:

```bash
$ docker-compose down -v
```