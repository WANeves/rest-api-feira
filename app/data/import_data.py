import json
import logging
import os
import sys

from typing import Dict, List

import pandas as pd

from pandas import DataFrame

from app.database.models.distrito import DistritoModel
from app.database.models.endereco import EnderecoModel
from app.database.models.feira import FeiraModel
from app.database.models.sub_prefeitura import SubPrefeituraModel
from app.models import SubPrefeitura, Distrito
from app.models.endereco import Endereco
from app.models.feira import Feira


CUR_DIR = os.path.abspath(os.path.dirname(__file__))


def import_file_data_to_database(flask_app):

    DISTRITO = ['CODDIST', 'DISTRITO', 'CODSUBPREF']
    SUB_PREFEITURA = ['CODSUBPREF', 'SUBPREFE', 'REGIAO5', 'REGIAO8']
    FEIRA = ['ID', 'NOME_FEIRA',  'REGISTRO', ]
    ENDERECO = ['LONG', 'LAT', 'SETCENS', 'LOGRADOURO', 'NUMERO', 'BAIRRO', 'AREAP', 'REFERENCIA', 'CODDIST', 'ID']

    file_name = f"{CUR_DIR}/DEINFO_AB_FEIRASLIVRES_2014.csv"
    df = pd.read_csv(file_name)

    feira_df = get_dataframe_columns(FEIRA, df)
    distrito_df = get_dataframe_columns(DISTRITO, df)
    sub_pref_df = get_dataframe_columns(SUB_PREFEITURA, df)
    endereco_df = get_dataframe_columns(ENDERECO, df)

    sub_prefeituras = get_group_by_sub_prefeitura(sub_pref_df)
    distritos = get_group_by_distrito(distrito_df)
    enderecos = get_group_by_endereco(endereco_df)

    save_sub_prefeituras(flask_app, sub_prefeituras)
    save_distritos(flask_app, distritos)
    save_enderecos(flask_app, enderecos)
    save_feiras(feira_df, flask_app)

    # current_app.logger.debug(f"{feira}")
    # current_app.logger.debug(f"{distrito}")
    # current_app.logger.debug(f"{sub_pref}")
    # current_app.logger.debug(f"{endereco}")


def get_group_by_sub_prefeitura(sub_pref_df: DataFrame):

    subs = []
    group_by = sub_pref_df.groupby("CODSUBPREF")

    for id, group in group_by:
        item = group.iloc[0]

        sub = create_sub_prefeitura(
            id, item.SUBPREFE, item.REGIAO5, item.REGIAO8
        )

        subs.append(sub)

    return subs


def get_group_by_distrito(distrito_df: DataFrame):

    distritos = []
    group_by = distrito_df.groupby("CODDIST")

    for id, group in group_by:
        item = group.iloc[0]

        sub = create_distrito(
            id, item.DISTRITO, item.CODSUBPREF
        )

        distritos.append(sub)

    return distritos


def get_group_by_endereco(endereco_df: DataFrame):

    distritos = []
    group_by = endereco_df.groupby("ID")

    for id, group in group_by:
        item = group.iloc[0]

        sub = create_endereco(
            id, item.LOGRADOURO, item.NUMERO, item.BAIRRO, item.REFERENCIA, item.LAT,
            item.LONG, item.SETCENS, item.AREAP, item.CODDIST
        )

        distritos.append(sub)

    return distritos


def create_sub_prefeitura(id, name: str, region_5: str, region_8: str):

    sub_prefeitura = SubPrefeitura()
    sub_prefeitura.id = id
    sub_prefeitura.name = name
    sub_prefeitura.region_5 = region_5
    sub_prefeitura.region_8 = region_8

    return sub_prefeitura


def create_distrito(id, name: str, sub_prefeitura_id: int):
    distrito = Distrito()
    distrito.id = id
    distrito.name = name
    distrito.sub_prefeitura_id = int(sub_prefeitura_id)

    return distrito


def create_endereco(
        id, logradouro: str, numero: str, bairro: str,
        referencia: str, lat: str, long: str, set_cens: str,
        area_p: str, distrito_id: int
):
    endereco = Endereco()
    endereco.id = id
    endereco.logradouro = logradouro
    endereco.numero = numero
    endereco.bairro = bairro
    endereco.referencia = referencia
    endereco.latitude = str(lat)
    endereco.longitude = str(long)
    endereco.set_cens = str(set_cens)
    endereco.area_p = str(area_p)
    endereco.distrito_id = int(distrito_id)

    return endereco


def create_feira(id: int, name: str, registro: str, endereco_id: str):
    feira = Feira()
    feira.id = id
    feira.name = name
    feira.registro = registro
    feira.endereco_id = endereco_id

    return feira


def get_dataframe_columns(columns, df):
    return df[[i for i in list(df.columns) if i in columns]]


def resource_json(json_: Dict, type_: str):
    item_id = json_["id"]

    json_.pop("id", None)

    json_resource = {
        "data": {
            "type": type_,
            "id": item_id,
            "attributes": json_
        },
        "type": "string"
    }

    return json_resource


def save_sub_prefeituras(flask_app, sub_prefeituras: List):

    logging.info(f"[i] Starting to import data from SubPrefeituras!")

    for sub in sub_prefeituras:
        with flask_app.app_context():
            SubPrefeituraModel.from_obj(sub).save()

    logging.info(f"[✓] SubPrefeituras imported and saved successfully!")


def save_distritos(flask_app, distritos: List):
    logging.info(f"[i] Starting to import data from Distritos!")

    for sub in distritos:
        with flask_app.app_context():
            DistritoModel.from_obj(sub).save()

    logging.info(f"[✓] Distritos imported and saved successfully!")


def save_enderecos(flask_app, enderecos: List):
    logging.info(f"[i] Starting to import data from Enderecos!")

    for sub in enderecos:
        with flask_app.app_context():
            EnderecoModel.from_obj(sub).save()

    logging.info(f"[✓] Enderecos imported and saved successfully!")


def save_feiras(feira_df, flask_app):
    logging.info(f"[i] Starting to import data from Feiras!")

    for _, row in feira_df.iterrows():
        feira = Feira()
        feira.id = int(row["ID"])
        feira.name = row["NOME_FEIRA"]
        feira.registro = row["REGISTRO"]
        feira.endereco_id = feira.id

        with flask_app.app_context():
            FeiraModel.from_obj(feira).save()

    logging.info(f"[✓] Feiras imported and saved successfully!")


if __name__ == "__main__":
    import_file_data_to_database()
