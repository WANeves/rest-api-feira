import logging

from flask import Flask
from flask_migrate import Migrate, upgrade, stamp
from flask_sqlalchemy import SQLAlchemy


def run_migration(app: Flask, db: SQLAlchemy) -> None:
    """ Run migrate in database
    Args:
        app (Flask):
        db (SQLAlchemy):
    """
    migration_path = app.config["MIGRATION_PATH"]

    # app.db = db
    migrate = Migrate(app, db, migration_path)
    migrate.init_app(app, db)

    with app.app_context():
        try:
            upgrade(directory=migration_path)
        except Exception as exc:
            logging.error("Migration execution failed.")
            db.drop_all()
            # stamp(directory=migration_path, revision="initial")
            # upgrade(directory=migration_path)
        finally:
            logging.info("Migrate successfully executed and completed.")
