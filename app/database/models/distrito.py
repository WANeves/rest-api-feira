from app.database.db import db
from app.models import Distrito


class DistritoModel(db.Model):
    """Distrito resource model

    Args:
        db (Object): SqlAlchemy metadata
    """

    __tablename__ = "distritos"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))

    sub_prefeituras_id = db.Column(
        db.Integer,
        db.ForeignKey("sub_prefeituras.id", ondelete="CASCADE"),
        nullable=True,
    )
    sub_prefeitura = db.relationship("SubPrefeituraModel", backref=db.backref("distritos"))

    @classmethod
    def from_obj(cls, obj: Distrito):
        return cls(
            id=obj.id,
            name=obj.name,
            sub_prefeituras_id=obj.sub_prefeitura_id,
        )

    def save(self):
        db.session.add(self)
        db.session.commit()
