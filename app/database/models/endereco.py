from app.database.db import db
from app.models.endereco import Endereco


class EnderecoModel(db.Model):
    """Endereco resource model

    Args:
        db (Object): SqlAlchemy metadata
    """

    __tablename__ = "enderecos"

    id = db.Column(db.Integer, primary_key=True)
    logradouro = db.Column(db.String(256))
    numero = db.Column(db.String(64))
    bairro = db.Column(db.String(256))
    referencia = db.Column(db.String(256))
    latitude = db.Column(db.String(256))
    longitude = db.Column(db.String(256))
    set_cens = db.Column(db.String(256))
    area_p = db.Column(db.String(256))

    distritos_id = db.Column(
        db.Integer,
        db.ForeignKey("distritos.id", ondelete="CASCADE"),
        nullable=True,
    )
    distrito = db.relationship("DistritoModel", backref=db.backref("enderecos"))

    @classmethod
    def from_obj(cls, obj: Endereco):
        return cls(
            id=obj.id,
            logradouro=obj.logradouro,
            numero=obj.numero,
            bairro=obj.bairro,
            referencia=obj.referencia,
            latitude=obj.latitude,
            longitude=obj.longitude,
            set_cens=obj.set_cens,
            area_p=obj.area_p,
            distritos_id=obj.distrito_id,
        )

    def save(self):
        db.session.add(self)
        db.session.commit()
