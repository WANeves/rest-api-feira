from app.database.db import db
from app.models.feira import Feira


class FeiraModel(db.Model):
    """Feira resource model

    Args:
        db (Object): SqlAlchemy metadata
    """

    __tablename__ = "feiras"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    registro = db.Column(db.String(64))

    enderecos_id = db.Column(
        db.Integer,
        db.ForeignKey("enderecos.id", ondelete="CASCADE"),
        nullable=True,
    )
    endereco = db.relationship("EnderecoModel", backref=db.backref("feiras"))

    @classmethod
    def from_obj(cls, obj: Feira):
        return cls(
            id=obj.id,
            name=obj.name,
            registro=obj.registro,
            enderecos_id=obj.endereco_id
        )

    def save(self):
        db.session.add(self)
        db.session.commit()
