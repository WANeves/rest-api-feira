from app.database.db import db
from app.models import SubPrefeitura


class SubPrefeituraModel(db.Model):
    """SubPrefeitura resource model

    Args:
        db (Object): SqlAlchemy metadata
    """

    __tablename__ = "sub_prefeituras"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    region_5 = db.Column(db.String(64), nullable=True)
    region_8 = db.Column(db.String(64), nullable=True)

    @classmethod
    def from_obj(cls, obj: SubPrefeitura):
        return cls(
            id=obj.id,
            name=obj.name,
            region_5=obj.region_5,
            region_8=obj.region_8,
        )

    def save(self):
        db.session.add(self)
        db.session.commit()

