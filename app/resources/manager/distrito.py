from flask_rest_jsonapi import (
    ResourceList,
    ResourceDetail,
    ResourceRelationship
)
from flask_rest_jsonapi.exceptions import ObjectNotFound
from sqlalchemy.orm.exc import NoResultFound

from app.database.db import db
from app.resources.schema.distrito import DistritoSchema
from app.database.models.distrito import DistritoModel
from app.resources.manager.util.util_distrito import check_values
from app.database.models.sub_prefeitura import SubPrefeituraModel


class DistritoList(ResourceList):
    """Distrito resource list manager.

    Args:
        ResourceList (Object): Base class of a resource list manager
    """

    def query(self, view_kwargs):
        query_ = self.session.query(DistritoModel)
        if view_kwargs.get("id") is not None:
            try:
                self.session.query(SubPrefeituraModel).filter_by(id=view_kwargs["id"]).one()
            except NoResultFound:
                raise ObjectNotFound(
                    {"parameter": "id"},
                    "SubPrefeitura: {} not found".format(view_kwargs["id"]),
                )
            else:
                query_ = query_.join(SubPrefeituraModel).filter(SubPrefeituraModel.id == view_kwargs["id"])
        return query_

    def before_create_object(self, data, view_kwargs):
        if view_kwargs.get("id") is not None:
            sub_prefeitura = self.session.query(SubPrefeituraModel).filter_by(id=view_kwargs["id"]).one()
            data["sub_prefeituras_id"] = sub_prefeitura.id

    schema = DistritoSchema
    data_layer = {
        "session": db.session,
        "model": DistritoModel,
        "methods": {"query": query, "before_create_object": before_create_object},
    }


class DistritoDetail(ResourceDetail):
    """Distrito detail manager

    Args:
        ResourceDetail (Object): Base class of a resource detail manager
    """

    def before_get_object(self, view_kwargs):
        check_values(view_kwargs, self.session)

    schema = DistritoSchema
    data_layer = {
        "session": db.session,
        "model": DistritoModel,
        "methods": {"before_get_object": before_get_object}
    }


class DistritoRelationship(ResourceRelationship):

    schema = DistritoSchema
    data_layer = {"session": db.session, "model": DistritoModel}
