from flask_rest_jsonapi import (
    ResourceList,
    ResourceDetail,
    ResourceRelationship
)

from app.database.db import db
from app.resources.manager.util.util_sub_prefeitura import check_values
from app.resources.schema.sub_prefeitura import (
    SubPrefeituraSchema
)
from app.database.models.sub_prefeitura import SubPrefeituraModel


class SubPrefeituraList(ResourceList):
    """SubPrefeitura resource list manager.

    Args:
        ResourceList (Object): Base class of a resource list manager
    """

    schema = SubPrefeituraSchema
    data_layer = {"session": db.session, "model": SubPrefeituraModel}


class SubPrefeituraDetail(ResourceDetail):
    """SubPrefeitura detail manager

    Args:
        ResourceDetail (Object): Base class of a resource detail manager
    """

    def before_get_object(self, view_kwargs):
        check_values(view_kwargs, self.session)

    schema = SubPrefeituraSchema
    data_layer = {
        "session": db.session,
        "model": SubPrefeituraModel,
        "methods": {"before_get_object": before_get_object},
    }


class SubPrefeituraRelationship(ResourceRelationship):
    """SubPrefeitura relationship"""

    schema = SubPrefeituraSchema
    data_layer = {"session": db.session, "model": SubPrefeituraModel}
