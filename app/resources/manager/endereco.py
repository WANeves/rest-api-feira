from flask_rest_jsonapi import (
    ResourceList,
    ResourceDetail,
    ResourceRelationship
)
from flask_rest_jsonapi.exceptions import ObjectNotFound
from sqlalchemy.orm.exc import NoResultFound

from app.database.db import db
from app.database.models.distrito import DistritoModel
from app.resources.schema.endereco import EnderecoSchema
from app.database.models.endereco import EnderecoModel
from app.resources.manager.util.util_endereco import check_values


class EnderecoList(ResourceList):
    """Endereco resource list manager.

    Args:
        ResourceList (Object): Base class of a resource list manager
    """

    def query(self, view_kwargs):
        query_ = self.session.query(EnderecoModel)
        if view_kwargs.get("id") is not None:
            try:
                self.session.query(DistritoModel).filter_by(id=view_kwargs["id"]).one()
            except NoResultFound:
                raise ObjectNotFound(
                    {"parameter": "id"},
                    "Distrito: {} not found in database".format(view_kwargs["id"]),
                )
            else:
                query_ = query_.join(DistritoModel).filter(DistritoModel.id == view_kwargs["id"])
        return query_

    def before_create_object(self, data, view_kwargs):
        if view_kwargs.get("id") is not None:
            distrito = self.session.query(DistritoModel).filter_by(id=view_kwargs["id"]).one()
            data["distritos_id"] = distrito.id

    schema = EnderecoSchema
    data_layer = {
        "session": db.session,
        "model": EnderecoModel,
        "methods": {"query": query, "before_create_object": before_create_object},
    }


class EnderecoDetail(ResourceDetail):
    """Endereco detail manager

    Args:
        ResourceDetail (Object): Base class of a resource detail manager
    """

    def before_get_object(self, view_kwargs):
        check_values(view_kwargs, self.session)

    schema = EnderecoSchema
    data_layer = {
        "session": db.session,
        "model": EnderecoModel,
        "methods": {"before_get_object": before_get_object}
    }


class EnderecoRelationship(ResourceRelationship):

    schema = EnderecoSchema
    data_layer = {"session": db.session, "model": EnderecoModel}
