from flask_rest_jsonapi import (
    ResourceList,
    ResourceDetail,
    ResourceRelationship
)
from flask_rest_jsonapi.exceptions import ObjectNotFound
from sqlalchemy.orm.exc import NoResultFound

from app.database.db import db
from app.database.models.endereco import EnderecoModel
from app.resources.schema.feira import FeiraSchema
from app.database.models.feira import FeiraModel
from app.resources.manager.util.util_feira import check_values


class FeiraList(ResourceList):
    """Feira resource list manager.

    Args:
        ResourceList (Object): Base class of a resource list manager
    """

    def query(self, view_kwargs):
        query_ = self.session.query(FeiraModel)
        if view_kwargs.get("id") is not None:
            try:
                self.session.query(EnderecoModel).filter_by(id=view_kwargs["id"]).one()
            except NoResultFound:
                raise ObjectNotFound(
                    {"parameter": "id"},
                    "Endereco: {} not found in database".format(view_kwargs["id"]),
                )
            else:
                query_ = query_.join(EnderecoModel).filter(EnderecoModel.id == view_kwargs["id"])
        return query_

    def before_create_object(self, data, view_kwargs):
        if view_kwargs.get("id") is not None:
            endereco = self.session.query(EnderecoModel).filter_by(id=view_kwargs["id"]).one()
            data["enderecos_id"] = endereco.id

    schema = FeiraSchema
    data_layer = {
        "session": db.session,
        "model": FeiraModel,
        "methods": {"query": query, "before_create_object": before_create_object},
    }


class FeiraDetail(ResourceDetail):
    """Feira detail manager

    Args:
        ResourceDetail (Object): Base class of a resource detail manager
    """

    def before_get_object(self, view_kwargs):
        check_values(view_kwargs, self.session)

    schema = FeiraSchema
    data_layer = {
        "session": db.session,
        "model": FeiraModel,
        "methods": {"before_get_object": before_get_object}
    }


class FeiraRelationship(ResourceRelationship):

    schema = FeiraSchema
    data_layer = {"session": db.session, "model": FeiraModel}
