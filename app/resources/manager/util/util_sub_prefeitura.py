
from flask_rest_jsonapi.exceptions import ObjectNotFound
from sqlalchemy.orm.exc import NoResultFound

from app.database.models.distrito import DistritoModel
from app.database.models.sub_prefeitura import SubPrefeituraModel


def check_values(view_kwargs, session):
    if view_kwargs.get("distrito_id") is not None:
        set_id_model(view_kwargs, session)
    elif view_kwargs.get("id") is not None:
        find_model_exists(view_kwargs, session)


def set_id_model(view_kwargs, session):
    try:
        distrito = (
            session.query(DistritoModel)
                .filter_by(id=view_kwargs["distrito_id"])
                .one()
        )
    except NoResultFound:
        message_not_found(
            "distrito_id",
            "Distrito",
            view_kwargs["distrito_id"]
        )
    else:
        view_kwargs["id"] = None
        if distrito.sub_prefeitura is None:
            raise ObjectNotFound(
                "Distrito {} does not have a related address".format(distrito.id, id),
            )
        else:
            view_kwargs["id"] = distrito.sub_prefeitura.id


def message_not_found(field_name: str, class_name: str, id: int):
    raise ObjectNotFound(
        {"parameter": f"{field_name}"},
        "{}: {} not found in database".format(class_name, id),
    )


def find_model_exists(view_kwargs, session):
    id = view_kwargs["id"]

    query = session.query(SubPrefeituraModel).filter(SubPrefeituraModel.id == id)
    exists = session.query(query.exists()).scalar()

    if not exists:
        message_not_found("id", "SubPrefeitura", id)
