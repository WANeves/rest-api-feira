
from flask_rest_jsonapi.exceptions import ObjectNotFound
from sqlalchemy.orm.exc import NoResultFound

from app.database.models.endereco import EnderecoModel


def check_values(view_kwargs, session):
    if view_kwargs.get("feira_id") is not None:
        set_id_model(view_kwargs, session)
    elif view_kwargs.get("id") is not None:
        find_model_exists(view_kwargs, session)


def set_id_model(view_kwargs, session):
    try:
        endereco = (
            session.query(EnderecoModel)
                .filter_by(id=view_kwargs["endereco_id"])
                .one()
        )
    except NoResultFound:
        message_not_found(
            "endereco_id",
            "Endereco",
            view_kwargs["endereco_id"]
        )
    else:
        # NESTE CENÁRIO NÃO É UTILIZADO PELA FEIRA
        view_kwargs["id"] = None
        if endereco.distrito is not None:
            view_kwargs["id"] = endereco.distrito.id


def message_not_found(field_name: str, class_name: str, id: int):
    raise ObjectNotFound(
        {"parameter": f"{field_name}"},
        "{}: {} not found in database".format(class_name, id),
    )


def find_model_exists(view_kwargs, session):
    id = view_kwargs["id"]

    query = session.query(EnderecoModel).filter(EnderecoModel.id == id)
    exists = session.query(query.exists()).scalar()

    if not exists:
        message_not_found("id", "Endereco", id)


