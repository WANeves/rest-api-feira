from flask import Flask
from flask_rest_jsonapi import Api

from app.resources.manager.distrito import (
    DistritoList,
    DistritoDetail,
    DistritoRelationship
)
from app.resources.manager.endereco import (
    EnderecoList,
    EnderecoDetail,
    EnderecoRelationship
)
from app.resources.manager.feira import (
    FeiraList,
    FeiraDetail,
    FeiraRelationship
)
from app.resources.manager.sub_prefeitura import (
    SubPrefeituraList,
    SubPrefeituraDetail,
    SubPrefeituraRelationship
)


def set_resources(app: Flask):
    """Expose domain objects as REST resources.

    Args:
        app (Object): Flask instance
    """

    api = Api(app)
    prefix = app.config["PREFIX"]

    with app.app_context():
        api.route(SubPrefeituraList,
                  "sub_prefeitura_list",
                  f"{prefix}/sub-prefeituras/")

        api.route(
            SubPrefeituraDetail,
            "sub_prefeitura_detail",
            f"{prefix}/sub-prefeituras/<string:id>/",
            f"{prefix}/distritos/<string:distrito_id>/sub-prefeitura",
        )

        api.route(
            SubPrefeituraRelationship,
            "sub_prefeitura_distritos",
            f"{prefix}/sub-prefeituras/<string:id>/relationships/distritos",
        )

        # --- DISTRITOS ---
        api.route(
            DistritoList,
            "distrito_list",
            f"{prefix}/distritos/",
            f"{prefix}/sub-prefeituras/<string:id>/distritos",
        )

        api.route(
            DistritoDetail,
            "distrito_detail",
            f"{prefix}/distritos/<string:id>/",
            f"{prefix}/enderecos/<string:endereco_id>/distrito",
        )

        api.route(
            DistritoRelationship,
            "distritos_sub_prefeituras",
            f"{prefix}/distritos/<string:id>/relationships/sub-prefeituras",
        )

        api.route(
            DistritoRelationship,
            "distritos_enderecos",
            f"{prefix}/distritos/<string:id>/relationships/enderecos",
        )


        # --- ENDERECOS ---
        api.route(
            EnderecoList,
            "endereco_list",
            f"{prefix}/enderecos/",
            f"{prefix}/distritos/<string:id>/enderecos",
        )

        api.route(
            EnderecoDetail,
            "endereco_detail",
            f"{prefix}/enderecos/<string:id>/",
            f"{prefix}/feiras/<string:feira_id>/endereco",
        )

        api.route(
            EnderecoRelationship,
            "enderecos_distritos",
            f"{prefix}/enderecos/<string:id>/relationships/distritos",
        )

        api.route(
            EnderecoRelationship,
            "enderecos_feiras",
            f"{prefix}/enderecos/<string:id>/relationships/feiras",
        )

        # --- FEIRAS ---
        api.route(
            FeiraList,
            "feira_list",
            f"{prefix}/feiras/",
            f"{prefix}/enderecos/<string:id>/feiras",
        )

        api.route(
            FeiraDetail,
            "feira_detail",
            f"{prefix}/feiras/<string:id>/",
        )

        api.route(
            FeiraRelationship,
            "feiras_enderecos",
            f"{prefix}/feiras/<string:id>/relationships/enderecos",
        )
