from marshmallow_jsonapi import fields
from marshmallow_jsonapi.flask import Relationship

from app.common.baseschema import BaseSchema


class EnderecoSchema(BaseSchema):
    """Endereco resource schema

    Args:
        Schema (Object): Flask schema
    """

    class Meta:
        type_ = "Endereco"
        self_view = "endereco_detail"
        self_view_kwargs = {"id": "<id>"}
        self_view_many = "endereco_list"

    id = fields.Int(required=False)
    logradouro = fields.Str(required=False)
    numero = fields.Str(required=False)
    bairro = fields.Str(required=False)
    referencia = fields.Str(required=False)
    latitude = fields.Str(required=False)
    longitude = fields.Str(required=False)
    set_cens = fields.Str(required=False)
    area_p = fields.Str(required=False)

    distrito = Relationship(
        attribute="distrito",
        self_view="enderecos_distritos",
        self_view_kwargs=dict(id="<id>"),
        related_view="distrito_detail",
        related_view_kwargs=dict(endereco_id="<id>"),
        schema="DistritoSchema",
        type_="Distrito",
    )

    feiras = Relationship(
        self_view="enderecos_feiras",
        self_view_kwargs=dict(id="<id>"),
        related_view="feira_list",
        related_view_kwargs=dict(id="<id>"),
        many=True,
        schema="FeiraSchema",
        type_="Feira",
    )
