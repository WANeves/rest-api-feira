from marshmallow_jsonapi import fields
from marshmallow_jsonapi.flask import Relationship

from app.common.baseschema import BaseSchema


class SubPrefeituraSchema(BaseSchema):
    """SubPrefeitura resource schema

    Args:
        Schema (Object): A Flask schema that resolves self URLs from view names
    """

    class Meta:
        type_ = "SubPrefeitura"
        self_view = "sub_prefeitura_detail"
        self_view_kwargs = {"id": "<id>"}
        self_view_many = "sub_prefeitura_list"

    id = fields.Int(required=False)
    name = fields.Str(required=False)
    region_5 = fields.Str(required=False)
    region_8 = fields.Str(required=False)

    distritos = Relationship(
        self_view="sub_prefeitura_distritos",
        self_view_kwargs=dict(id="<id>"),
        related_view="distrito_list",
        related_view_kwargs=dict(id="<id>"),
        many=True,
        schema="DistritoSchema",
        type_="Distrito",
    )


