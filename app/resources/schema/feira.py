from marshmallow_jsonapi import fields
from marshmallow_jsonapi.flask import Relationship

from app.common.baseschema import BaseSchema


class FeiraSchema(BaseSchema):
    """Feira resource schema

    Args:
        Schema (Object): Flask schema
    """

    class Meta:
        type_ = "Feira"
        self_view = "feira_detail"
        self_view_kwargs = {"id": "<id>"}
        self_view_many = "feira_list"

    id = fields.Int(required=False)
    name = fields.Str(required=False)
    registro = fields.Str(required=False)

    endereco = Relationship(
        attribute="endereco",
        self_view="feiras_enderecos",
        self_view_kwargs=dict(id="<id>"),
        related_view="endereco_detail",
        related_view_kwargs=dict(feira_id="<id>"),
        schema="EnderecoSchema",
        type_="Endereco",
    )
