from marshmallow_jsonapi import fields
from marshmallow_jsonapi.flask import Relationship

from app.common.baseschema import BaseSchema


class DistritoSchema(BaseSchema):
    """Distrito resource schema

    Args:
        Schema (Object): A Flask schema that resolves self URLs from view names
    """

    class Meta:
        type_ = "Distrito"
        self_view = "distrito_detail"
        self_view_kwargs = {"id": "<id>"}
        self_view_many = "distrito_list"

    id = fields.Int(required=False)
    name = fields.Str(required=False)

    sub_prefeitura = Relationship(
        attribute="sub_prefeitura",
        self_view="distritos_sub_prefeituras",
        self_view_kwargs=dict(id="<id>"),
        related_view="sub_prefeitura_detail",
        related_view_kwargs=dict(distrito_id="<id>"),
        schema="SubPrefeituraSchema",
        type_="SubPrefeitura",
    )

    enderecos = Relationship(
        self_view="distritos_enderecos",
        self_view_kwargs=dict(id="<id>"),
        related_view="endereco_list",
        related_view_kwargs=dict(id="<id>"),
        many=True,
        schema="EnderecoSchema",
        type_="Endereco",
    )
