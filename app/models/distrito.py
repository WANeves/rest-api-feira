# coding: utf-8


class Distrito():
    """Distrito Model"""

    def __init__(
            self,
            id: int = None,
            name: str = None,
            sub_prefeitura_id: int = None,
    ):
        self._id = id
        self._name = name
        self._sub_prefeitura_id = sub_prefeitura_id

    @property
    def id(self) -> int:
        """Gets the id of this Distrito.

        :return: The id of this Distrito.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id: int):
        """Sets the id of this Distrito.

        :param id: The id of this Distrito.
        :type id: int
        """

        self._id = id

    @property
    def name(self) -> str:
        """Gets the name of this Distrito.

        :return: The name of this Distrito.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this Distrito.

        :param name: The name of this Distrito.
        :type name: str
        """

        self._name = name

    @property
    def sub_prefeitura_id(self) -> int:
        """Gets the sub_prefeitura_id of this Distrito.

        :return: The sub_prefeitura_id of this Distrito.
        :rtype: int
        """
        return self._sub_prefeitura_id

    @sub_prefeitura_id.setter
    def sub_prefeitura_id(self, sub_prefeitura_id: int):
        """Sets the sub_prefeitura_id of this Distrito.

        :param sub_prefeitura_id: The sub_prefeitura_id of this Distrito.
        :type sub_prefeitura_id: int
        """

        self._sub_prefeitura_id = sub_prefeitura_id
