# coding: utf-8


class Endereco():
    """Endereco Model"""

    def __init__(
            self,
            id: int = None,
            logradouro: str = None,
            numero: str = None,
            bairro: str = None,
            referencia: str = None,
            latitude: str = None,
            longitude: str = None,
            set_cens: str = None,
            are_p: str = None,
            distrito_id: int = None,
    ):
        self._id = id
        self._logradouro = logradouro
        self._numero = numero
        self._bairro = bairro
        self._referencia = referencia
        self._latitude = latitude
        self._longitude = longitude
        self._set_cens = set_cens
        self._area_p = are_p
        self._distrito_id = distrito_id

    @property
    def id(self) -> int:
        """Gets the id of this Endereco.

        :return: The id of this Endereco.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id: int):
        """Sets the id of this Endereco.

        :param id: The id of this Endereco.
        :type id: int
        """

        self._id = id

    @property
    def logradouro(self) -> str:
        """Gets the logradouro of this Endereco.

        :return: The logradouro of this Endereco.
        :rtype: str
        """
        return self._logradouro

    @logradouro.setter
    def logradouro(self, logradouro: str):
        """Sets the logradouro of this Endereco.

        :param logradouro: The logradouro of this Endereco.
        :type logradouro: str
        """

        self._logradouro = logradouro

    @property
    def numero(self) -> str:
        """Gets the numero of this Endereco.

        :return: The numero of this Endereco.
        :rtype: str
        """
        return self._numero

    @numero.setter
    def numero(self, numero: str):
        """Sets the numero of this Endereco.

        :param numero: The numero of this Endereco.
        :type numero: str
        """

        self._numero = numero

    @property
    def bairro(self) -> str:
        """Gets the bairro of this Endereco.

        :return: The bairro of this Endereco.
        :rtype: str
        """
        return self._bairro

    @bairro.setter
    def bairro(self, bairro: str):
        """Sets the bairro of this Endereco.

        :param bairro: The bairro of this Endereco.
        :type bairro: str
        """

        self._bairro = bairro

    @property
    def referencia(self) -> str:
        """Gets the referencia of this Endereco.

        :return: The referencia of this Endereco.
        :rtype: str
        """
        return self._referencia

    @referencia.setter
    def referencia(self, referencia: str):
        """Sets the referencia of this Endereco.

        :param referencia: The referencia of this Endereco.
        :type referencia: str
        """

        self._referencia = referencia

    @property
    def latitude(self) -> str:
        """Gets the latitude of this Endereco.

        :return: The latitude of this Endereco.
        :rtype: str
        """
        return self._latitude

    @latitude.setter
    def latitude(self, latitude: str):
        """Sets the latitude of this Endereco.

        :param latitude: The latitude of this Endereco.
        :type latitude: str
        """

        self._latitude = latitude

    @property
    def longitude(self) -> str:
        """Gets the longitude of this Endereco.

        :return: The longitude of this Endereco.
        :rtype: str
        """
        return self._longitude

    @longitude.setter
    def longitude(self, longitude: str):
        """Sets the longitude of this Endereco.

        :param longitude: The longitude of this Endereco.
        :type longitude: str
        """

        self._longitude = longitude

    @property
    def set_cens(self) -> str:
        """Gets the set_cens of this Endereco.

        :return: The set_cens of this Endereco.
        :rtype: str
        """
        return self._set_cens

    @set_cens.setter
    def set_cens(self, set_cens: str):
        """Sets the set_cens of this Endereco.

        :param set_cens: The set_cens of this Endereco.
        :type set_cens: str
        """

        self._set_cens = set_cens

    @property
    def area_p(self) -> str:
        """Gets the area_p of this Endereco.

        :return: The area_p of this Endereco.
        :rtype: str
        """
        return self._area_p

    @area_p.setter
    def area_p(self, area_p: str):
        """Sets the area_p of this Endereco.

        :param area_p: The area_p of this Endereco.
        :type area_p: str
        """

        self._area_p = area_p

    @property
    def distrito_id(self) -> int:
        """Gets the distrito_id of this Endereco.

        :return: The distrito_id of this Endereco.
        :rtype: int
        """
        return self._distrito_id

    @distrito_id.setter
    def distrito_id(self, distrito_id: int):
        """Sets the distrito_id of this Endereco.

        :param distrito_id: The distrito_id of this Endereco.
        :type distrito_id: int
        """

        self._distrito_id = distrito_id
