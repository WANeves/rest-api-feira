# coding: utf-8


class SubPrefeitura():
    """SubPrefeitura Model"""

    def __init__(
            self,
            id: int = None,
            name: str = None,
            region_5: str = None,
            region_8: str = None,
    ):
        self._id = id
        self._name = name
        self._region_5 = region_5
        self._region_8 = region_8

    @property
    def id(self) -> int:
        """Gets the id of this SubPrefeitura.

        :return: The id of this SubPrefeitura.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id: int):
        """Sets the id of this SubPrefeitura.

        :param id: The id of this SubPrefeitura.
        :type id: int
        """

        self._id = id

    @property
    def name(self) -> str:
        """Gets the name of this SubPrefeitura.

        :return: The name of this SubPrefeitura.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this SubPrefeitura.

        :param name: The name of this SubPrefeitura.
        :type name: str
        """

        self._name = name

    @property
    def region_5(self) -> str:
        """Gets the region_5 of this SubPrefeitura.

        :return: The region_5 of this SubPrefeitura.
        :rtype: str
        """
        return self._region_5

    @region_5.setter
    def region_5(self, region_5: str):
        """Sets the region_5 of this SubPrefeitura.

        :param region_5: The region_5 of this SubPrefeitura.
        :type region_5: str
        """

        self._region_5 = region_5

    @property
    def region_8(self) -> str:
        """Gets the region_8 of this SubPrefeitura.

        :return: The region_8 of this SubPrefeitura.
        :rtype: str
        """
        return self._region_8

    @region_8.setter
    def region_8(self, region_8: str):
        """Sets the region_8 of this SubPrefeitura.

        :param region_8: The region_8 of this SubPrefeitura.
        :type region_8: str
        """

        self._region_8 = region_8
