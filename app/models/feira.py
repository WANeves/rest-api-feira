# coding: utf-8
import util


class Feira():
    """Feira Model"""

    def __init__(
            self,
            id: int = None,
            name: str = None,
            registro: str =None,
            endereco_id: int = None,
    ):
        self._id = id
        self._name = name
        self._registro = registro
        self._endereco_id = endereco_id

    @property
    def id(self) -> int:
        """Gets the id of this Feira.

        :return: The id of this Feira.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id: int):
        """Sets the id of this Feira.

        :param id: The id of this Feira.
        :type id: int
        """

        self._id = id

    @property
    def name(self) -> str:
        """Gets the name of this Feira.

        :return: The name of this Feira.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this Feira.

        :param name: The name of this Feira.
        :type name: str
        """

        self._name = name

    @property
    def registro(self) -> str:
        """Gets the registro of this Feira.

        :return: The registro of this Feira.
        :rtype: str
        """
        return self._registro

    @registro.setter
    def registro(self, registro: str):
        """Sets the registro of this Feira.

        :param registro: The registro of this Feira.
        :type registro: str
        """

        self._registro = registro

    @property
    def endereco_id(self) -> int:
        """Gets the endereco_id of this Feira.

        :return: The endereco_id of this Feira.
        :rtype: int
        """
        return self._endereco_id

    @endereco_id.setter
    def endereco_id(self, endereco_id: int):
        """Sets the endereco_id of this Feira.

        :param endereco_id: The endereco_id of this Feira.
        :type endereco_id: int
        """

        self._endereco_id = endereco_id
