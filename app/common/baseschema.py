from marshmallow_jsonapi.flask import Schema, SchemaOpts


class BaseSchemaOpts(SchemaOpts):
    """Base SchemaOpts for all JSONApi resources."""

    def __init__(self, meta, *args, **kwargs):

        def dasherize(text):
            """
            Convert snake_case field names to jsonapi kebab-case.

            Args:
                text (str): field_name

            Returns:
                str: kebab-case-name
            """

            return text.replace('_', '-')

        meta.inflect = dasherize
        super().__init__(meta, *args, **kwargs)


class BaseSchema(Schema):
    """Base Schema for all JSONApi resources."""

    OPTIONS_CLASS = BaseSchemaOpts
