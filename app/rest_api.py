import os

from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate

from app.data.import_data import import_file_data_to_database
from app.database.db import db
from app.database.migrate import run_migration
from app.database.models.feira import FeiraModel
from app.database.models.sub_prefeitura import SubPrefeituraModel
from app.resources.routes.routes import set_resources
from app.swagger.swagger_config import set_swagger


def configure_app(flask_app: Flask) -> None:
    """Set config Flask instance
    Args:
        flask_app (Object): Flask instance
    """

    flask_app.config.from_object(
        f"app.config.{os.getenv('APPLICATION_ENV', 'Development')}"
    )


def create_app(is_test: bool = False) -> Flask:
    """Creates an instance of the Flask.
    Returns:
        Object: Flask instance
    """

    app = Flask(__name__)

    CORS(app)
    configure_app(app)
    set_database(app, is_test)
    set_resources(app)
    set_swagger(app)

    if not is_test:
        import_file_data_to_database(app)

    return app


def set_database(flask_app: Flask, is_test: bool = False) -> None:
    """Set config current database
    Args:
        flask_app (Object): Flask instance
        is_test: (bool)
    """
    db.app = flask_app
    db.init_app(flask_app)

    # with flask_app.app_context():
        # db.drop_all()

    run_migration(flask_app, db)

    if not is_test:
        run_migration(flask_app, db)
        clean_database(flask_app)

    # migrate = Migrate(flask_app, db)


def clean_database(flask_app):
    with flask_app.app_context():
        try:
            db.session.query(SubPrefeituraModel).delete()
            db.session.commit()
            db.session.query(FeiraModel).delete()
            db.session.commit()
        except:
            db.session.rollback()


app = create_app()

if __name__ == "__main__":
    app.run(port=app.config["PORT"])
