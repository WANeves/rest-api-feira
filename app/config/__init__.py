import os
from pathlib import Path


class Config(object):
    REPOSITORY_PATH = Path(__file__).resolve().parent.parent.parent
    DEBUG = True
    
    PREFIX = "/api/v1/rest"
    PORT = 8080
    HOST = "0.0.0.0"
    PAGE_SIZE = 20

    PROPAGATE_EXCEPTIONS = True
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SWAGGER_PATH = os.path.join(REPOSITORY_PATH, "app", "swagger/swagger.json")
    MIGRATION_PATH = os.path.join(REPOSITORY_PATH, "migrations")


class Development(Config):

    FLASK_ENV = "development"
    ENV = FLASK_ENV

    SQLALCHEMY_DATABASE_URI = "postgresql://admin:admin@db:5432/feira_db"


class Testing(Config):
    SQLALCHEMY_DATABASE_URI = "sqlite:////tmp/test.db"
    FLASK_ENV = "development"


class Production(Config):
    pass

