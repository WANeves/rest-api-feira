import json
from typing import Dict

from flask import Flask
from flask.views import View
from flask_swagger_ui import get_swaggerui_blueprint


class SwaggerView(View):
    """Set view to swagger"""

    def __init__(self, conf_swagger: Dict):
        self._swagger = conf_swagger["template"]
        self._swagger["host"] = conf_swagger["host"]
        self._swagger["basePath"] = conf_swagger["base_path"]

    def dispatch_request(self):
        return json.dumps(self._swagger)


def set_url_swagger(flask_app: Flask) -> None:
    """Set URL swagger
    Args:
        flask_app (Object): Flask instance
    """
    file_path = open(flask_app.config["SWAGGER_PATH"])
    swagger = json.load(file_path)

    conf_swagger = get_conf_swagger(flask_app, swagger=swagger)

    flask_app.add_url_rule(
        f"{conf_swagger['base_path']}/swagger.json",
        view_func=SwaggerView.as_view(
            "swagger_view", conf_swagger
        ),
    )


def set_blueprint_in_swagger(flask_app: Flask) -> None:
    """Set config blueprint in swagger.
    Args:
        flask_app (Object): Flask instance
    """

    conf = get_conf_swagger(flask_app)

    host_name = f"http://{conf['host']}"
    endpoint = f"{host_name}{conf['base_path']}/swagger.json"

    blueprint = get_swaggerui_blueprint(
        conf['base_path'],
        endpoint,
        config={
            "docExpansion": "none",
            "defaultModelsExpandDepth": -1,
        },
    )

    flask_app.register_blueprint(blueprint, url_prefix=conf['base_path'])


def get_conf_swagger(flask_app: Flask, swagger=None) -> Dict:
    """Get the swagger access configuration.
    Args:
        flask_app (Object): Flask instance
        swagger:
    Returns
        Dict
    """

    prefix = flask_app.config["PREFIX"]
    host = flask_app.config["HOST"]
    port = flask_app.config["PORT"]

    if host == "0.0.0.0":
        host = "localhost"

    if port:
        host = "%s:%s" % (host, port)

    conf = {
        "template": swagger,
        "base_path": prefix,
        "host": host,
        "port": port,
    }

    return conf


def set_swagger(flask_app: Flask) -> None:
    """Set all config swagger.
    Args:
        flask_app (Object): Flask instance
    """
    set_url_swagger(flask_app)
    set_blueprint_in_swagger(flask_app)
